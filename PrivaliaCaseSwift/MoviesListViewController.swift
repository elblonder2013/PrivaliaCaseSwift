//
//  MoviesListViewController.swift
//  PrivaliaCaseSwift
//
//  Created by alexei on 4/21/18.
//  Copyright © 2018 privalia. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class MoviesListViewController: UIViewController{
    
    
    @IBOutlet weak var tableViewFilms: UITableView!
    @IBOutlet weak var tryAgainBtn: UIButton!
    @IBOutlet weak var loadResultView: UIView!
    var arrayFilms =  [NSDictionary]();
    var page = 1;
    var loading = false;
    var loadingView:LoadingView? = nil
    var dataWasLoaded = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //disabling Alamofire Cache, remove this line if you want to store the request result in cache
        URLCache.shared = URLCache(memoryCapacity: 0, diskCapacity: 0, diskPath: nil)
        
        self.tryAgainBtn.layer.borderColor = UIColor.black.cgColor
        
        self.tableViewFilms.delegate = self;
        self.tableViewFilms.dataSource = self;
        self.tableViewFilms.rowHeight = UITableViewAutomaticDimension
        self.tableViewFilms.estimatedRowHeight = 151
        let footerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 375, height: 30));
        self.tableViewFilms.tableFooterView = footerView;
        self.loadingView = UINib.init(nibName: "loadingCircles", bundle: nil).instantiate(withOwner: self)[0] as? LoadingView
        self.loadingView?.frame.size = CGSize.init(width: 30, height: 30)
        self.loadingView?.frame.origin = CGPoint.init(x: self.view.frame.size.width/2-15, y: self.view.frame.size.height/2-15);
        self.view.addSubview(loadingView!)
        self.loadingView?.startAnimate()
        
        
    }
   
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if !dataWasLoaded {
            self.getMoviesFromApiRest()
        }
    }
    @IBAction func TryAgain(_ sender: Any) {
          self.loadingView?.isHidden = false
          self.loadResultView.isHidden = true
          self.loadingView?.startAnimate()
          self.getMoviesFromApiRest()
    }
    
    
    
    func getMoviesFromApiRest(){
        let parameters: Parameters =  ["page": page,"limit":10,"extended":"full"]
        let headers: HTTPHeaders = [
            "content-type": "application/json",
            "trakt-api-version": "2",
            "trakt-api-key": "019a13b1881ae971f91295efc7fdecfa48b32c2a69fe6dd03180ff59289452b8"
        ]
        loading = true
        Alamofire.request("https://api.trakt.tv/movies/popular",  parameters: parameters, headers: headers).responseJSON { response in
            
            self.loading = false
            self.loadingView?.stopAnimate()
            self.loadingView?.Reset()
            self.loadingView?.isHidden = true
            if response.result.error != nil
            {
                
                if !self.dataWasLoaded
                {
                    self.tableViewFilms.isHidden = true
                    self.loadResultView.isHidden = false
                }
                else
                {
                    let alert = UIAlertController(title: "Error", message: "An error ocurred loading the movies", preferredStyle: .alert)
                     alert.addAction(UIAlertAction(title: NSLocalizedString("Try again", comment: "Default action"), style: .default, handler: { _ in
                     self.getMoviesFromApiRest()
                     }))
                     alert.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel action"), style: .default, handler: { _ in
                        self.loadingView?.stopAnimate()
                        self.loadingView?.Reset()
                        self.loadingView?.isHidden = true
                     }))
                     self.present(alert, animated: true, completion: nil)
                }
                
               
            }
            else if let json = response.result.value  {
                
                if !self.dataWasLoaded
                {
                    self.tableViewFilms.isHidden = false
                    self.loadResultView.isHidden = true
                    self.dataWasLoaded = true
                    self.tableViewFilms.tableFooterView?.addSubview(self.loadingView!);
                    self.loadingView?.frame.size = CGSize.init(width: 30, height: 30)
                    self.loadingView?.frame.origin = CGPoint.init(x: (self.tableViewFilms.tableFooterView?.frame.size.width)!/2-15, y: 0);
                   
                }
                if (json as! [NSDictionary]).count>0{
                    self.page = self.page+1
                    self.arrayFilms.append(contentsOf: json as! [NSDictionary])
                    self.tableViewFilms.reloadData()
                }
               
            }
        }
    }
    func stopAllRequests(){
        if #available(iOS 9.0, *) {
            Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
                tasks.forEach{ $0.cancel() }
            }
        } else {
            Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
                sessionDataTask.forEach { $0.cancel() }
                uploadData.forEach { $0.cancel() }
                downloadData.forEach { $0.cancel() }
            }
        }
    }
     
    @IBAction func SearchMoview(_ sender: Any) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SEARCHUI") as! SearchMovesViewController
        vc.view.backgroundColor = UIColor.init(red: 50/255, green: 50/255, blue: 50/255, alpha: 0.5)
        vc.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
        let navigationController = UINavigationController.init(rootViewController: vc)
        navigationController.isNavigationBarHidden = true
        navigationController.view.backgroundColor = UIColor.clear
        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext;
        self.present(navigationController, animated: false, completion: nil)
    }
 
   
    
    
}

// MARK: - TableView DataSource -------------------
extension MoviesListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayFilms.count
    }
}
// MARK: - TableView Delegate -------------------
extension MoviesListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellFilmID", for: indexPath) as! CellFilm
        let dictFilm = arrayFilms[indexPath.row]
        cell.titleLabel?.text = (dictFilm.value(forKey: "title") as! String)
        cell.overviewLabel.text = (dictFilm.value(forKey: "overview") as! String)
        cell.yearLabel.text =  NSString.init(format: "%@", dictFilm.value(forKey: "year") as! CVarArg) as String
        let dictIds = dictFilm["ids"] as! NSDictionary
        if let imdbID = dictIds["imdb"] {
            let urlForPoster = "http://img.omdbapi.com/?apikey=985eef78&i=" + (imdbID as! String)
            cell.filmImageView.sd_setImage(with: URL.init(string: urlForPoster), placeholderImage: UIImage.init(named: "lauch_image"))
        }
        else
        {
            cell.filmImageView.image = UIImage.init(named: "lauch_image")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DETAILSUI") as! DetailsMovieViewController
        vc.movieDetails = self.arrayFilms[indexPath.row];
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if self.loading {
            return;
        }
        if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height
        {
            let difference = (scrollView.contentOffset.y + scrollView.frame.size.height)-scrollView.contentSize.height
            print(difference)
            if difference > 0 && !(loadingView?.shouldAnimate)!
            {
                loadingView?.Reset()
                loadingView?.isHidden = false
                if difference <= 33
                {
                    loadingView?.circle1.alpha = 1
                }
                else if difference > 33 && difference <= 66
                {
                    loadingView?.circle1.alpha = 1
                    loadingView?.circle2.alpha = 1
                }
                else
                {
                    loadingView?.circle1.alpha = 1
                    loadingView?.circle2.alpha = 1
                    loadingView?.circle3.alpha = 1
                    loadingView?.startAnimate()
                    self.getMoviesFromApiRest()
                  
                }
                
            }
            else
            {
                loadingView?.isHidden = true
            }
            
        }
    }
}

