//
//  DetailsMovieViewController.swift
//  PrivaliaCaseSwift
//
//  Created by alexei on 4/22/18.
//  Copyright © 2018 privalia. All rights reserved.
//

import UIKit
import SDWebImage
class DetailsMovieViewController: UIViewController {
    @IBOutlet weak var containerScroll: UIScrollView!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var overViewLabel: UILabel!
    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    @IBOutlet weak var bannerImageView: UIImageView!
    @IBOutlet weak var playButton: UIButton!
    
    @IBOutlet weak var start1Image: UIImageView!
    @IBOutlet weak var start2Image: UIImageView!
    @IBOutlet weak var start3Image: UIImageView!
    @IBOutlet weak var start4Image: UIImageView!
    @IBOutlet weak var start5Image: UIImageView!
    
   
    var movieDetails: NSDictionary = [:]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if movieDetails["trailer"] != nil && !(movieDetails["trailer"] is NSNull)
        {
            self.playButton.isHidden = false;
        }
        
        self.titleLabel.text = (movieDetails.value(forKey: "title") as! String)
        self.overViewLabel.text = (movieDetails.value(forKey: "overview") as! String)
        self.reviewLabel.text = String.init(format: "%.1f", movieDetails["rating"] as! Float)
        
        //Showing the stars rating
        let arrayStart = [start1Image,start2Image,start3Image,start4Image,start5Image]
        let raitingInt = Int((movieDetails["rating"] as! Float)/2)
        for i in 1...raitingInt {
            arrayStart[i-1]?.image = UIImage.init(named: "start_black")
        }
        
        let arrgenres = movieDetails["genres"] as! NSArray
        if arrgenres.count>0
        {
            self.genderLabel.text = arrgenres[0] as? String
            if arrgenres.count>1
            {
                for i in 1...arrgenres.count-1 {
                    self.genderLabel.text = String.init(format: "%@, %@",  self.genderLabel.text! ,(arrgenres[i] as? String)!)
                }
            }
        }
        
        let dictIds = movieDetails["ids"] as! NSDictionary
        if dictIds["imdb"] != nil && !(dictIds["imdb"] is NSNull) {
            let imdbID = dictIds["imdb"]
            let urlForPoster = "http://img.omdbapi.com/?apikey=985eef78&i=" + (imdbID as! String)
            posterImageView.sd_setImage(with: URL.init(string: urlForPoster), placeholderImage: UIImage.init(named: "lauch_image"))
              bannerImageView.sd_setImage(with: URL.init(string: urlForPoster), placeholderImage: UIImage.init(named: "lauch_image"))
            
        }
        else
        {
           posterImageView.image = UIImage.init(named: "lauch_image")
        }
       
    }
    @IBAction func PlayTrailer(_ sender: Any) {
        let trailerUrl = movieDetails.value(forKey: "trailer") as! String
        
        if trailerUrl.contains("youtube")
        {
            let components = trailerUrl.components(separatedBy: "=")
            let youtubeId = components.last!
            
            if let youtubeURL = URL(string: "youtube://\(youtubeId)"),
                UIApplication.shared.canOpenURL(youtubeURL) {
                // redirect to app
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(youtubeURL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(youtubeURL)
                }
            } else if let youtubeURL = URL(string: "https://www.youtube.com/watch?v=\(youtubeId)") {
                // redirect through safari
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(youtubeURL, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(youtubeURL)
                }
            }
        }
    }
    @IBAction func GoBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
