//
//  SearchMovesViewController.swift
//  PrivaliaCaseSwift
//
//  Created by alexei on 4/22/18.
//  Copyright © 2018 privalia. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
class SearchMovesViewController: UIViewController {

    @IBOutlet weak var topBarView: UIView!
    @IBOutlet weak var searchingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var resultTaleView: UITableView!
    @IBOutlet weak var searchResultView: UIView!
    @IBOutlet weak var searchResultImage: UIImageView!
    @IBOutlet weak var searchResultLabel: UILabel!
    
    
    var searching:Bool = false
    var keyBoardHeight:CGFloat = 0.0
    var searchPage = 1
    var arrayFilms =  [NSDictionary]();
    var pullRefreshValue = 100
    var loadingView:LoadingView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        //disabling Alamofire Cache, remove this line if you want to store the request result in cache
        URLCache.shared = URLCache(memoryCapacity: 0, diskCapacity: 0, diskPath: nil)
        
        self.topBarView.frame.origin.y -=  self.topBarView.frame.height;
        self.searchTextField.delegate = self;
        self.resultTaleView.delegate = self;
        self.resultTaleView.dataSource = self;
        self.resultTaleView.rowHeight = UITableViewAutomaticDimension
        self.resultTaleView.estimatedRowHeight = 151
        self.searchingIndicator.isHidden = true
        let footerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.frame.size.width, height: 30));
        self.resultTaleView.tableFooterView = footerView;
        self.loadingView = UINib.init(nibName: "loadingCircles", bundle: nil).instantiate(withOwner: self)[0] as? LoadingView
        self.loadingView?.frame.size = CGSize.init(width: 30, height: 30)
        self.loadingView?.frame.origin = CGPoint.init(x: footerView.frame.size.width/2-15, y: 0);
        footerView.addSubview(loadingView!)
        self.loadingView?.isHidden = true
        
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if self.keyBoardHeight == 0.0 {
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
            self.searchTextField.becomeFirstResponder()
        }
        if self.topBarView.frame.origin.y < 0
        {
            UIView.animate(withDuration: 0.3, animations: {
                self.topBarView.frame.origin.y = 0
            })
        }
        
       
       
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            if self.keyBoardHeight == 0.0
            {
                let keyboardHeightGet = keyboardSize.height
                self.keyBoardHeight = keyboardHeightGet
                var rect = self.resultTaleView.frame;
                rect.size.height = rect.size.height - self.keyBoardHeight+1;
                resultTaleView.frame = rect
            }
            
        }
    }
    
    @IBAction func Close(_ sender: UIButton) {
        self.stopAllRequests()
        UIView.animate(withDuration: 0.3, animations: {
            sender.superview?.frame.origin.y = (sender.superview?.frame.origin.y)!-(sender.superview?.frame.size.height)!-20
            
            self.view.alpha = 0
        }) { (finisehd) in
            self.searchTextField.resignFirstResponder()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    
    func findMoviesFromApiRest(fromSearch: Bool, queryString:String){
        
        
        let parameters: Parameters =  ["limit":10,"extended":"full"]
        let headers: HTTPHeaders = [
            "content-type": "application/json",
            "trakt-api-version": "2",
            "trakt-api-key": "019a13b1881ae971f91295efc7fdecfa48b32c2a69fe6dd03180ff59289452b8"
        ]
        searching = true
        self.searchingIndicator.isHidden = false;
        self.searchingIndicator.startAnimating()
        var pageAux:Int = self.searchPage;
        if !fromSearch {
            pageAux += 1
        }
        let url = String.init(format: "https://api.trakt.tv/movies/popular?page=%i&query=%@", pageAux,queryString).addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        
        Alamofire.request(url!,  parameters: parameters, headers: headers).responseJSON { response in
            self.searching = false
            self.searchingIndicator.isHidden = true
            if response.result.error != nil
            {
                let errorDescription = response.result.error?.localizedDescription
                if errorDescription != "cancelled"
                {
                    self.resultTaleView.isHidden = true
                    self.searchResultView.isHidden = false
                    self.searchResultLabel.text = "Upss... something was wrong"
                    self.searchResultImage.image = UIImage.init(named: "search_status_error")
                }
                
                
            }
            else if let json = response.result.value  {
                if fromSearch
                {
                    self.arrayFilms = json as! [NSDictionary]
                    self.searchPage = 1
                }
                else{
                
                    if (json as! [NSDictionary]).count>0{
                        self.arrayFilms.append(contentsOf: json as! [NSDictionary])
                        self.searchPage = pageAux
                    }
                   
                  
                }
                if self.arrayFilms.count>0 {
                   self.resultTaleView.isHidden = false
                   self.searchResultView.isHidden = true
                }
                else {
                    self.resultTaleView.isHidden = true
                    self.searchResultView.isHidden = false
                    self.searchResultLabel.text = "Your search don't have results"
                    self.searchResultImage.image = UIImage.init(named: "search_status_not_found")
                }
                self.resultTaleView.reloadData()
                self.loadingView?.stopAnimate()
                self.loadingView?.Reset()
                self.loadingView?.isHidden = true
            }
        }
    }
    func stopAllRequests(){
        
        if #available(iOS 9.0, *) {
            Alamofire.SessionManager.default.session.getAllTasks { (tasks) in
                tasks.forEach{ $0.cancel() }
            }
        } else {
            Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
                sessionDataTask.forEach { $0.cancel() }
                uploadData.forEach { $0.cancel() }
                downloadData.forEach { $0.cancel() }
            }
        }
    }
    
}

// MARK: - ResultTableView delegate -------------------
extension SearchMovesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayFilms.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellFilmID", for: indexPath) as! CellFilm
        let dictFilm = arrayFilms[indexPath.row]
        
        if dictFilm["title"] != nil && !(dictFilm["title"] is NSNull)
        {
             cell.titleLabel?.text = (dictFilm.value(forKey: "title") as! String)
        }
        if dictFilm["overview"] != nil && !(dictFilm["overview"] is NSNull)
        {
            cell.overviewLabel.text = (dictFilm.value(forKey: "overview") as! String)
        }
        if dictFilm["year"] != nil && !(dictFilm["year"] is NSNull)
        {
            cell.yearLabel.text =  NSString.init(format: "%@", dictFilm.value(forKey: "year") as! CVarArg) as String
        }
       
        let dictIds = dictFilm["ids"] as! NSDictionary
        if dictIds["imdb"] != nil && !(dictIds["imdb"] is NSNull){
            let imdbID = dictIds["imdb"] as! String
            let urlForPoster = "http://img.omdbapi.com/?apikey=985eef78&i=" + (imdbID)
                cell.filmImageView.sd_setImage(with: URL.init(string: urlForPoster), placeholderImage: UIImage.init(named: "lauch_image"))
        }
        else
        {
            cell.filmImageView.image = UIImage.init(named: "lauch_image")
        }
        
        
        return cell
    }
}
    
   

// MARK: - ResultTableView delegate -------------------
extension SearchMovesViewController: UITableViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
     
        if self.searching {
            return;
        }
         if (scrollView.contentOffset.y + scrollView.frame.size.height) >= scrollView.contentSize.height
         {
            let difference = (scrollView.contentOffset.y + scrollView.frame.size.height)-scrollView.contentSize.height
            if difference > 0 && !(loadingView?.shouldAnimate)!
            {
                loadingView?.isHidden = false
                loadingView?.Reset()
                if difference <= 33
                {
                    loadingView?.circle1.alpha = 1
                }
                else if difference > 33 && difference <= 66
                {
                    loadingView?.circle1.alpha = 1
                    loadingView?.circle2.alpha = 1
                }
                else
                {                    
                    loadingView?.circle1.alpha = 1
                    loadingView?.circle2.alpha = 1
                    loadingView?.circle3.alpha = 1                    
                    loadingView?.startAnimate()
                    self.findMoviesFromApiRest(fromSearch: false, queryString: self.searchTextField.text!)
                }
                
            }
            else
            {
                loadingView?.isHidden = true
            }
           
         }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "DETAILSUI") as! DetailsMovieViewController
        vc.movieDetails = self.arrayFilms[indexPath.row];
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

// MARK: - SearchTextField delegate -------------------
extension SearchMovesViewController: UITextFieldDelegate
{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
      
        let typeCasteToStringFirst = textField.text as NSString?
        let newString =  typeCasteToStringFirst?.replacingCharacters(in: range, with: string)
        print( String.init(format: "%@", newString!))
        if searching
        {
            self.stopAllRequests()
            searching = false
        }
        self.searchPage = 1;
        self.findMoviesFromApiRest(fromSearch: true, queryString: String.init(format: "%@", newString!))
        return true;
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if self.keyBoardHeight > 0.0 {
            self.resultTaleView.frame.size.height = self.view.frame.size.height - self.resultTaleView.frame.origin.y - self.keyBoardHeight+1;
           
        }
        return true;
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.resultTaleView.frame.size.height =   self.resultTaleView.frame.size.height + self.keyBoardHeight-1;
        return true;
    }  
}
