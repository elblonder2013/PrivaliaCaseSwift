//
//  CellFilm.swift
//  PrivaliaCaseSwift
//
//  Created by alexei on 4/21/18.
//  Copyright © 2018 privalia. All rights reserved.
//

import UIKit

class CellFilm: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    @IBOutlet weak var filmImageView: UIImageView!    

}
