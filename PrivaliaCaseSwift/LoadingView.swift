//
//  LoadingView.swift
//  PrivaliaCaseSwift
//
//  Created by alexei on 4/23/18.
//  Copyright © 2018 privalia. All rights reserved.
//

import UIKit

class LoadingView: UIView {

    @IBOutlet weak var circle1: UIView!
    @IBOutlet weak var circle2: UIView!
    @IBOutlet weak var circle3: UIView!
    var shouldAnimate:Bool = false
    
    func Reset()
    {
        self.circle1.alpha  = 0.33
        self.circle2.alpha  = 0.33
        self.circle3.alpha  = 0.33
        self.circle1.transform = CGAffineTransform.init(scaleX: 1, y: 1)
        self.circle2.transform = CGAffineTransform.init(scaleX: 1, y: 1)
        self.circle3.transform = CGAffineTransform.init(scaleX: 1, y: 1)
    }
    func animate(flag:Bool) -> Void {
        UIView.animate(withDuration: 0.4, animations: {
            if flag {
                self.circle2.transform = CGAffineTransform.init(scaleX: 0.3, y: 0.3)
                self.circle1.transform = CGAffineTransform.init(scaleX: 1, y: 1)
                self.circle3.transform = CGAffineTransform.init(scaleX: 1, y: 1)
                
                self.circle2.alpha = 0.33
                self.circle1.alpha = 1
                self.circle3.alpha = 1
            }
            else
            {
                self.circle2.transform = CGAffineTransform.init(scaleX: 1, y: 1)
                self.circle1.transform = CGAffineTransform.init(scaleX: 0.3, y: 0.3)
                self.circle3.transform = CGAffineTransform.init(scaleX: 0.3, y: 0.3)
                
                self.circle2.alpha = 1
                self.circle1.alpha = 0.33
                self.circle3.alpha = 0.33
            }
           
        }) { (finished) in
            if self.shouldAnimate
            {
                self.animate(flag: !flag)
            }
        }
    }
  
    func stopAnimate()
    {
        self.shouldAnimate = false;
    }
    func startAnimate()
    {
        self.shouldAnimate = true
        self.animate(flag: true)
    }
   
    
   
    
    
}



